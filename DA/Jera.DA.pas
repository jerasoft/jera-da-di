{******************************************************************************}
{ Projeto: TJeraDA                                                             }
{                                                                              }
{ Fun��o: Gerar arquivo de remessa e efetuar a a leitura de mesmo, referente   }
{         D�bitos Autorizados/Autom�tico                                       }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{******************************************************************************}

{******************************************************************************}
{ Direitos Autorais Reservados � 2016 - J�ter Rabelo Ferreira                  }
{ Contato: jeter.rabelo@jerasoft.com.br                                        }
{******************************************************************************}
unit Jera.DA;

interface

uses
  System.Classes, System.SysUtils, System.Rtti, System.Generics.Collections, Jera.DADI.Util;

type
  TDARemessaRetorno = (rrRemessa, rrRetorno);

  TJeraDABancos = (daNenhum,
                   daBradesco,
                   daItau,
                   daCEF);

  TDACodigoMoeda = (cmUfir, cmReal);

  TDACodigoMovimento = (cmoDebito, cmoCancelamento, cmoCadastroOptantes);

//  TDACodigoRetorno = (ccr00DebitoEfetuado,
//                      ccr01DebitonaoEfetuadoInsuficienciadeFundos,
//                      ccr02DebitonaoEfetuadoContaNaoCadastrada,
//                      ccr04DebitonaoEfetuadoOutrasRestricoes,
//                      ccr05DebitonaoEfetuadoValorExcedeLimiteAprovado,
//                      ccr10DebitonaoEfetuadoAgenciaRegimeEncerramento,
//                      ccr12DebitonaoEfetuadoValorInvalido,
//                      ccr13DebitonaoEfetuadoDataLancamentoInvalida,
//                      ccr14DebitonaoEfetuadoAgenciaInvalida,
//                      ccr15DebitonaoEfetuadoContaInvalida,
//                      ccr18DebitonaoEfetuadoDataInferiorProcessamento,
//                      ccr19DebitonaoEfetuadoContaNaoPertenceCNPJCPF,
//                      ccr20DebitonaoEfetuadoContaConjuntaNaoSolidaria,
//                      ccr30DebitonaoEfetuadoSemContratoDebitoAutomatico,
//                      ccr31DebitoEfetuadoDataDiferenteFeriadoPracaDevito,
//                      ccr47DebitoNaoEfetuadoValorDebitoAcimaLimite,
//                      ccr48DebitoNaoEfetuadoLimiteDiarioDebitoUltrapassado,
//                      ccr49DebitoNaoEfetuadoCnpjCpfDebitadoInvalido,
//                      ccr50DebitoNaoEfetuadoCnpjCpfNaoPertenceContaDbitada,
//                      ccr96ManutencaoCadastro,
//                      ccr97CancelamentoNaoEncontrado,
//                      ccr98CancelamentoMaoEfetuadoForaTempoHabil,
//                      ccr99CancelamentoCanceladoConformeSolicitacao,
//                      ccrAQTipoQtdeMoedaInvalida,
//                      ccrIDValorMoraInvallido,
//                      ccrBDConfirmacaoAgendamento,
//                      ccrPEDebitoPendenteAutorizacao,
//                      ccrNADebitoNaoAutorizado,
//                      ccrATDebitoAutorizado,
//                      ccrRCDebitoRecusao);
//
//  TDACodigoRetornoDS = record helper for TDACodigoRetorno
//    function ToString: string;
//  end;

  TDACodigoMovimentoBlocoB = (cmbbExclusao,
                              cmbbInclusao,
                              cmddOutros);
  TDACodigoMovimentoBlocoBHelper = record helper for TDACodigoMovimentoBlocoB
    function ToString: string;
  end;

//  TDAOcorrenciaBlocoC = (obcIdentificacaoDoCLienteNaoLocalizada,
//                         obcRestricaoDeCadastramentoPelaEmpresa,
//                         obcClienteCadastradoEmOutroBancoComDataPosterior,
//                         obcClienteDesativadoNoCadastroDaEmpresa,
//                         obcOperadoraInvalida);

// TDAOcorrenciaBlocoCH = record helper for TDAOcorrenciaBlocoC
//   function ToString: string;
// end;

//const
//  TDAOcorrenciaBlocoCDS: array[0..4] of string =
//                         ('Identifica��o do cliente n�o localizada',
//                          'Restricao de Cadastramento Pela Empresa',
//                          'Cliente cadastrado em outro banco com data posterior',
//                          'Cliente desativado no cadastro da empresa',
//                          'Operadora Inv�lida');

type
  TDAOcorrenciaBlocoD = (obdExclusaoPorAlteracaoCadastralDoCliente,
                         obdExclusaoTransferidoParaDebitoEmOutroBanco,
                         cbdExclusaoPorInsuficienaDeFundos,
                         cdbExclusaoPorSolicitacaoDoCliente);

  TDACodigoMovimentoBlocoD = (cmbdAlteracao, cmdbExclusao);

  TDAAmbiente = (ambProducao, ambTeste);

//  Registro �A� - Header
//  Obrigat�rio em todos os arquivos.
  IJeraDABlocoA = Interface(IInterface)
    ['{91FFD5B4-8AED-43EE-94EA-053A6AA74C8B}']
    function GetCodigoRegistro: string;
    function GetCodigoRemessa: TDARemessaRetorno;
    procedure SetCodigoRemessa(const Value: TDARemessaRetorno);
    function GetCodigoConvenio: string;
    procedure SetCodigoConvenio(const Value: string);
    function GetNomedaEmpresa: string;
    procedure SetNomedaEmpresa(const Value: string);
    function GetCodigoBanco: string;
    function GetNomeBanco: string;
    function GetDataGeracao: TDate;
    procedure SetDataGeracao(const Value: TDate);
    function GetNSA: Integer;
    procedure SetNSA(const Value: Integer);
    function GetVersaoLayout: string;
    function GetIdentificacaoServico: string;
    function GetBrancos: string;
    function GetBrancos2: string;
    procedure SetBrancos2(const Value: string);
    function GetBrancos3: string;
    function GetContaCompromisso: string;
    procedure SetContaCompromisso(const Value: string);
    function GetIdentificacaoAmbienteCliente: TDAAmbiente;
    procedure SetIdentificacaoAmbienteCliente(const Value: TDAAmbiente);
    function GetIdentificacaoAmbienteBanco: TDAAmbiente;
    procedure SetIdentificacaoAmbienteBanco(const Value: TDAAmbiente);

    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property CodigoRemessa: TDARemessaRetorno read GetCodigoRemessa write SetCodigoRemessa;
    property CodigoConvenio: string read GetCodigoConvenio write SetCodigoConvenio;
    property NomedaEmpresa: string read GetNomedaEmpresa write SetNomedaEmpresa;
    property CodigoBanco: string read GetCodigoBanco;
    property NomeBanco: string read GetNomeBanco;
    property DataGeracao: TDate read GetDataGeracao write SetDataGeracao;
    property NSA: Integer read GetNSA write SetNSA;
    property VersaoLayout: string read GetVersaoLayout;
    property IdentificacaoServico: string read GetIdentificacaoServico;
    property Brancos: string read GetBrancos;
    property Brancos2: string read GetBrancos2 write SetBrancos2;
    property Brancos3: string read GetBrancos3;
    property ContaCompromisso: string read GetContaCompromisso write SetContaCompromisso;
    property IdentificacaoAmbienteCliente: TDAAmbiente read GetIdentificacaoAmbienteCliente write SetIdentificacaoAmbienteCliente;
    property IdentificacaoAmbienteBanco: TDAAmbiente read GetIdentificacaoAmbienteBanco write SetIdentificacaoAmbienteBanco;
  end;

//  Registro �B� - Cadastramento de D�bito Autom�tico
//  Gerado pelo Banco para a Empresa, para cada inclus�o ou exclus�o, de optante pelo d�bito autom�tico.
  IJeraDABlocoB = Interface(IInterface)
    ['{54B9E225-C965-4BFF-823D-84AF973C9ED9}']
    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetData: TDate;
    procedure SetData(const Value: TDate);
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoB;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoB);

    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property Data: TDate read GetData write SetData;
    property Brancos: string read GetBrancos;
    property Brancos2: string read GetBrancos2;
    property CodigoMovimento: TDACodigoMovimentoBlocoB read GetCodigoMovimento write SetCodigoMovimento;
  end;

//  Registro �C� - Ocorr�ncias no Cadastramento do D�bito Autom�tico
//  Gerado obrigatoriamente pela Empresa para o Banco, somente para cada �cadastramento� (registro �B�), enviado pelo
//  Banco, que for recusado pela Empresa. EM HIP�TESE ALGUMA DEVE SER GERADO PARA OS REGISTROS
//  ACEITOS.
  IJeraDABlocoC = Interface(IInterface)
    ['{BFAF20F9-5843-4B3A-8ED6-9B91205214A1}']
    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetBrancos1: string;
    procedure SetBrancos1(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetOcorrencia: string;
    procedure SetOcorrencia(const Value: string);
    function GetOcorrencia2: string;
    procedure SetOcorrencia2(const Value: string);
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoB;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoB);

    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    function DescricaoOcorrencia: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property Brancos1: string read GetBrancos1 write SetBrancos1;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Ocorrencia: string read GetOcorrencia write SetOcorrencia;
    property Ocorrencia2: string read GetOcorrencia2 write SetOcorrencia2;
    property Brancos: string read GetBrancos;
    property Brancos2: string read GetBrancos2;
    property CodigoMovimento: TDACodigoMovimentoBlocoB read GetCodigoMovimento write SetCodigoMovimento;
  end;

//  Registro �D� - Altera��o da Identifica��o do Cliente na Empresa
//  Gerado pela Empresa para o Banco, obrigatoriamente, nas seguintes situa��es:
//  � Necessidade de altera��o, por parte da Empresa, da �Identifica��o do Cliente na Empresa�. Em cada registro ser�
//  informado o par: Identifica��o Anterior ou Identifica��o Atual (DE/PARA), e
//  � Nas situa��es onde a empresa �necessitar� excluir o cliente da modalidade de d�bito autom�tico.
  IJeraDABlocoD = Interface(IInterface)
    ['{25DF980B-76B4-4F93-8EBD-51BE6721D65B}']
    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresaAnterior: string;
    procedure SetIdentificacaoClienteEmpresaAnterior(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetOcorrencia: TDAOcorrenciaBlocoD;
    procedure SetOcorrencia(const Value: TDAOcorrenciaBlocoD);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoD;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimentoBlocoD);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;

    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresaAnterior: string read GetIdentificacaoClienteEmpresaAnterior write SetIdentificacaoClienteEmpresaAnterior;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property Ocorrencia: TDAOcorrenciaBlocoD read GetOcorrencia write SetOcorrencia;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoD read GetCodigoMovimento write SetCodigoMovimento;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos2: string read GetBrancos2;
  end;

//  Registro �E� - D�bito em Conta
//  Gerado pela Empresa para o Banco.
  IJeraDABlocoE = Interface(IInterface)
    ['{DC23C3A5-094B-4D28-90D5-3123A6EB547A}']
    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetDataVencimento: TDate;
    procedure SetDataVencimento(const Value: TDate);
    function GetValorDebito: Currency;
    procedure SetValorDebito(const Value: Currency);
    function GetCodigoMoeda: TDACodigoMoeda;
    procedure SetCodigoMoeda(const Value: TDACodigoMoeda);
    function GetUsoEmpresa: string;
    procedure SetUsoEmpresa(const Value: string);
    function GetTratamentoAcordado: Boolean;
    procedure SetTratamentoAcordado(const Value: Boolean);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetIdentificacao: string;
    procedure SetIdentificacao(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimento;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimento);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;
    procedure SetBrancos2(const Value: string);
    function GetValorMora: Currency;
    procedure SetValorMora(const Value: Currency);
    function GetComplemento: string;
    procedure SetComplemento(const Value: string);

    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property DataVencimento: TDate read GetDataVencimento write SetDataVencimento;
    property ValorDebito: Currency read GetValorDebito write SetValorDebito;
    property CodigoMoeda: TDACodigoMoeda read GetCodigoMoeda write SetCodigoMoeda;
    property UsoEmpresa: string read GetUsoEmpresa write SetUsoEmpresa;
    property TratamentoAcordado: Boolean read GetTratamentoAcordado write SetTratamentoAcordado;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property Identificacao: string read GetIdentificacao write SetIdentificacao;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimento read GetCodigoMovimento write SetCodigoMovimento;
    property Brancos2: string read GetBrancos2 write SetBrancos2;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property ValorMora: Currency read GetValorMora write SetValorMora;
    property Complemento: string read GetComplemento write SetComplemento;
  end;

//  Registro �F� - Retorno do D�bito Autom�tico
//  Gerado pelo Banco para a Empresa.
//  Ser� gerado um registro �F�, para cada registro de d�bito (registro �E�), enviado anteriormente.
  IJeraDABlocoF = Interface(IInterface)
    ['{59471675-08F5-4B84-9456-33C8F5A4E31C}']
    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetDataVencimento: TDate;
    procedure SetDataVencimento(const Value: TDate);
    function GetValorDebito: Currency;
    procedure SetValorDebito(const Value: Currency);
    function GetValorMora: Currency;
    procedure SetValorMora(const Value: Currency);
    function GetCodigoRetorno: string;
    procedure SetCodigoRetorno(const Value: string);
    function GetUsoEmpresa: string;
    procedure SetUsoEmpresa(const Value: string);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetIdentificacao: Int64;
    procedure SetIdentificacao(const Value: Int64);
    function GetBrancos: string;
    function GetBrancos2: string;
    function GetCodigoMovimento: TDACodigoMovimento;
    procedure SetCodigoMovimento(const Value: TDACodigoMovimento);

    function MontarLinha: string;
    function CodigoRetornoDescricao(const Value: string): string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property DataVencimento: TDate read GetDataVencimento write SetDataVencimento;
    property ValorDebito: Currency read GetValorDebito write SetValorDebito;
    property CodigoRetorno: string read GetCodigoRetorno write SetCodigoRetorno;
    property UsoEmpresa: string read GetUsoEmpresa write SetUsoEmpresa;
    property ValorMora: Currency read GetValorMora write SetValorMora;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property Identificacao: Int64 read GetIdentificacao write SetIdentificacao;
    property Brancos: string read GetBrancos;
    property Brancos2: string read GetBrancos2;
    property CodigoMovimento: TDACodigoMovimento read GetCodigoMovimento write SetCodigoMovimento;
  end;

//  Registro �H� - Ocorr�ncia de Altera��o da Identifica��o do Cliente na Empresa
//  Gerado pelo Banco para a Empresa, somente para cada �altera��o� (registro �D�), enviada pela Empresa, que for recusada
//  pelo Banco. EM HIP�TESE ALGUMA SER� GERADO PARA OS REGISTROS ACEITOS.
  IJeraDABlocoH = Interface(IInterface)
    ['{F89F7C84-6D2B-43F2-9ABB-7B146F8AD096}']
    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresaAnterior: string;
    procedure SetIdentificacaoClienteEmpresaAnterior(const Value: string);
    function GetAgenciaDebito: string;
    procedure SetAgenciaDebito(const Value: string);
    function GetIdentificacaoClienteBanco: string;
    procedure SetIdentificacaoClienteBanco(const Value: string);
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetOcorrencia: string;
    procedure SetOcorrencia(const Value: string);
    function GetBrancos: string;
    function GetCodigoMovimento: TDACodigoMovimentoBlocoD;
    procedure SetCodigoMovimento(Value: TDACodigoMovimentoBlocoD);
    function GetConta: string;
    procedure SetConta(const Value: string);
    function GetContaDigito: string;
    procedure SetContaDigito(const Value: string);
    function GetBrancos2: string;

    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresaAnterior: string read GetIdentificacaoClienteEmpresaAnterior write SetIdentificacaoClienteEmpresaAnterior;
    property AgenciaDebito: string read GetAgenciaDebito write SetAgenciaDebito;
    property IdentificacaoClienteBanco: string read GetIdentificacaoClienteBanco write SetIdentificacaoClienteBanco;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property Ocorrencia: string read GetOcorrencia write SetOcorrencia;
    property Brancos: string read GetBrancos;
    property CodigoMovimento: TDACodigoMovimentoBlocoD read GetCodigoMovimento write SetCodigoMovimento;
    property Conta: string read GetConta write SetConta;
    property ContaDigito: string read GetContaDigito write SetContaDigito;
    property Brancos2: string read GetBrancos2;
  end;

//  Registro �I� - Incentivo de D�bito Autom�tico
//  Gerado pela Empresa para o Banco.
//  Este registro dever� ser gerado somente para os consumidores que ainda n�o s�o optantes pelo D�bito Autom�tico. De
//  posse destas informa��es, o Banco ir� trabalhar para incentivar a ades�o ao D�bito Autom�tico.
//  A gera��o deste registro � opcional.
  IJeraDABlocoI = Interface(IInterface)
    ['{35B59111-4D81-401C-9ED4-A43D05936DAF}']
    function GetCodigoRegistro: string;
    function GetIdentificacaoClienteEmpresa: string;
    procedure SetIdentificacaoClienteEmpresa(const Value: string);
    function GetTipoIdentificacao: TDADIIdentificacao;
    procedure SetTipoIdentificacao(const Value: TDADIIdentificacao);
    function GetCnpjCpf: string;
    procedure SetCnpjCpf(const Value: string);
    function GetNomeConsumidor: string;
    procedure SetNomeConsumidor(const Value: string);
    function GetCidadeConsumidor: string;
    procedure SetCidadeConsumidor(const Value: string);
    function GetUFConsumidor: string;
    procedure SetUFConsumidor(const Value: string);
    function GetBrancos: string;

    function MontarLinha: string;
    property CodigoRegistro: string read GetCodigoRegistro;
    property IdentificacaoClienteEmpresa: string read GetIdentificacaoClienteEmpresa write SetIdentificacaoClienteEmpresa;
    property TipoIdentificacao: TDADIIdentificacao read GetTipoIdentificacao write SetTipoIdentificacao;
    property CnpjCpf: string read GetCnpjCpf write SetCnpjCpf;
    property NomeConsumidor: string read GetNomeConsumidor write SetNomeConsumidor;
    property CidadeConsumidor: string read GetCidadeConsumidor write SetCidadeConsumidor;
    property UFConsumidor: string read GetUFConsumidor write SetUFConsumidor;
    property Brancos: string read GetBrancos;
  end;

//  Registro �Z� - Trailler
//  Obrigat�rio em todos os arquivos.
  IJeraDABlocoZ = Interface(IInterface)
    ['{FA5CFDA1-2BC3-43D3-AEAE-88421D7423E6}']
    function GetCodigoRegistro: string;
    function GetTotalRegistrosArquivo: Integer;
    procedure SetTotalRegistrosArquivo(const Value: Integer);
    function GetValorTotalRegistros: Currency;
    procedure SetValorTotalRegistros(const Value: Currency);
    function GetBrancos: string;
    function GetBrancos1: string;
    procedure SetBrancos1(const Value: string);
    function GetBrancos2: string;

    function MontarLinha: string;
    procedure CarregarLinha(const Value: string);
    property CodigoRegistro: string read GetCodigoRegistro;
    property TotalRegistrosArquivo: Integer read GetTotalRegistrosArquivo write SetTotalRegistrosArquivo;
    property ValorTotalRegistros: Currency read GetValorTotalRegistros write SetValorTotalRegistros;
    property Brancos: string read GetBrancos;
    property Brancos1: string read GetBrancos1 write SetBrancos1;
    property Brancos2: string read GetBrancos2;
  end;

  IJeraDARemessa = Interface(IInterface)
    ['{FC1D4141-374F-4A6D-84D5-5748CA3355BA}']
    function GetBlocoA: IJeraDABlocoA;
    procedure SetBlocoA(const Value: IJeraDABlocoA);
    function GetBlocoC: TDADIClasseItemsBase<IJeraDABlocoC>;
    procedure SetBlocoC(const Value: TDADIClasseItemsBase<IJeraDABlocoC>);
    function GetBlocoD: TDADIClasseItemsBase<IJeraDABlocoD>;
    procedure SetBlocoD(const Value: TDADIClasseItemsBase<IJeraDABlocoD>);
    function GetBlocoE: TDADIClasseItemsBase<IJeraDABlocoE>;
    procedure SetBlocoE(const Value: TDADIClasseItemsBase<IJeraDABlocoE>);
    function GetBlocoZ: IJeraDABlocoZ;
    procedure SetBlocoZ(const Value: IJeraDABlocoZ);
    function GetArquivoRemessa: string;

    procedure Processar;
    property BlocoA: IJeraDABlocoA read GetBlocoA write SetBlocoA;
    property BlocoC: TDADIClasseItemsBase<IJeraDABlocoC> read GetBlocoC write SetBlocoC;
    property BlocoD: TDADIClasseItemsBase<IJeraDABlocoD> read GetBlocoD write SetBlocoD;
    property BlocoE: TDADIClasseItemsBase<IJeraDABlocoE> read GetBlocoE write SetBlocoE;
    property BlocoZ: IJeraDABlocoZ read GetBlocoZ write SetBlocoZ;
    property ArquivoRemessa: string read GetArquivoRemessa;
  end;

  IJeraDARetorno = Interface(IInterface)
    ['{0AB76A10-FEA9-49CB-9205-7EF788FDC19F}']
    function GetBlocoA: IJeraDABlocoA;
    procedure SetBlocoA(const Value: IJeraDABlocoA);
    function GetBlocoC: TDADIClasseItemsBase<IJeraDABlocoC>;
    procedure SetBlocoC(const Value: TDADIClasseItemsBase<IJeraDABlocoC>);
    function GetBlocoB: TDADIClasseItemsBase<IJeraDABlocoB>;
    procedure SetBlocoB(const Value: TDADIClasseItemsBase<IJeraDABlocoB>);
    function GetBlocoF: TDADIClasseItemsBase<IJeraDABlocoF>;
    procedure SetBlocoF(const Value: TDADIClasseItemsBase<IJeraDABlocoF>);
    function GetBlocoH: TDADIClasseItemsBase<IJeraDABlocoH>;
    procedure SetBlocoH(const Value: TDADIClasseItemsBase<IJeraDABlocoH>);
    function GetBlocoZ: IJeraDABlocoZ;
    procedure SetBlocoZ(const Value: IJeraDABlocoZ);

    procedure Processar(AFileName: string);
    property BlocoA: IJeraDABlocoA read GetBlocoA write SetBlocoA;
    property BlocoB: TDADIClasseItemsBase<IJeraDABlocoB> read GetBlocoB write SetBlocoB;
    property BlocoC: TDADIClasseItemsBase<IJeraDABlocoC> read GetBlocoC write SetBlocoC;
    property BlocoF: TDADIClasseItemsBase<IJeraDABlocoF> read GetBlocoF write SetBlocoF;
    property BlocoH: TDADIClasseItemsBase<IJeraDABlocoH> read GetBlocoH write SetBlocoH;
    property BlocoZ: IJeraDABlocoZ read GetBlocoZ write SetBlocoZ;
  end;

  TJeraDARemessa = class(TInterfacedObject, IJeraDARemessa)
  private
    FBanco: TJeraDABancos;
    FBlocoA: IJeraDABlocoA;
    FBlocoC: TDADIClasseItemsBase<IJeraDABlocoC>;
    FBlocoD: TDADIClasseItemsBase<IJeraDABlocoD>;
    FBlocoE: TDADIClasseItemsBase<IJeraDABlocoE>;
    FBlocoZ: IJeraDABlocoZ;
    FArquivoRemessa: string;
    function GetBlocoA: IJeraDABlocoA;
    procedure SetBlocoA(const Value: IJeraDABlocoA);
    function GetBlocoC: TDADIClasseItemsBase<IJeraDABlocoC>;
    procedure SetBlocoC(const Value: TDADIClasseItemsBase<IJeraDABlocoC>);
    function GetBlocoD: TDADIClasseItemsBase<IJeraDABlocoD>;
    procedure SetBlocoD(const Value: TDADIClasseItemsBase<IJeraDABlocoD>);
    function GetBlocoE: TDADIClasseItemsBase<IJeraDABlocoE>;
    procedure SetBlocoE(const Value: TDADIClasseItemsBase<IJeraDABlocoE>);
    function GetBlocoZ: IJeraDABlocoZ;
    procedure SetBlocoZ(const Value: IJeraDABlocoZ);
    function GetArquivoRemessa: string;
    procedure __IncluirSequencial(var AStr: TStringList;
                                  const ADigitos: byte;
                                  const AInicio: byte);
  public
    constructor Create(Banco: TJeraDABancos);
    destructor Destroy; override;
    procedure Processar;
    property BlocoA: IJeraDABlocoA read GetBlocoA write SetBlocoA;
    property BlocoC: TDADIClasseItemsBase<IJeraDABlocoC> read GetBlocoC write SetBlocoC;
    property BlocoD: TDADIClasseItemsBase<IJeraDABlocoD> read GetBlocoD write SetBlocoD;
    property BlocoE: TDADIClasseItemsBase<IJeraDABlocoE> read GetBlocoE write SetBlocoE;
    property BlocoZ: IJeraDABlocoZ read GetBlocoZ write SetBlocoZ;
    property ArquivoRemessa: string read GetArquivoRemessa;
  end;

  TJeraDARetorno = class(TInterfacedObject, IJeraDARetorno)
  private
    FBanco: TJeraDABancos;
    FBlocoA: IJeraDABlocoA;
    FBlocoB: TDADIClasseItemsBase<IJeraDABlocoB>;
    FBlocoC: TDADIClasseItemsBase<IJeraDABlocoC>;
    FBlocoF: TDADIClasseItemsBase<IJeraDABlocoF>;
    FBlocoH: TDADIClasseItemsBase<IJeraDABlocoH>;
    FBlocoZ: IJeraDABlocoZ;
    function GetBlocoA: IJeraDABlocoA;
    procedure SetBlocoA(const Value: IJeraDABlocoA);
    function GetBlocoB: TDADIClasseItemsBase<IJeraDABlocoB>;
    procedure SetBlocoB(const Value: TDADIClasseItemsBase<IJeraDABlocoB>);
    function GetBlocoC: TDADIClasseItemsBase<IJeraDABlocoC>;
    procedure SetBlocoC(const Value: TDADIClasseItemsBase<IJeraDABlocoC>);
    function GetBlocoF: TDADIClasseItemsBase<IJeraDABlocoF>;
    procedure SetBlocoF(const Value: TDADIClasseItemsBase<IJeraDABlocoF>);
    function GetBlocoH: TDADIClasseItemsBase<IJeraDABlocoH>;
    procedure SetBlocoH(const Value: TDADIClasseItemsBase<IJeraDABlocoH>);
    function GetBlocoZ: IJeraDABlocoZ;
    procedure SetBlocoZ(const Value: IJeraDABlocoZ);
  public
    constructor Create(Banco: TJeraDABancos);
    destructor Destroy; override;
    procedure Processar(AFileName: string);
    property BlocoA: IJeraDABlocoA read GetBlocoA write SetBlocoA;
    property BlocoB: TDADIClasseItemsBase<IJeraDABlocoB> read GetBlocoB write SetBlocoB;
    property BlocoC: TDADIClasseItemsBase<IJeraDABlocoC> read GetBlocoC write SetBlocoC;
    property BlocoF: TDADIClasseItemsBase<IJeraDABlocoF> read GetBlocoF write SetBlocoF;
    property BlocoH: TDADIClasseItemsBase<IJeraDABlocoH> read GetBlocoH write SetBlocoH;
    property BlocoZ: IJeraDABlocoZ read GetBlocoZ write SetBlocoZ;
  end;

implementation

uses
  System.StrUtils, System.TypInfo, ACBrUtil.Strings,
{$WARNINGS OFF}
  FileCtrl,
{$WARNINGS ON}
  Jera.DA.Bradesco,
  Jera.DA.CEF,
  Jera.DA.Itau;

{ TJeraDARemessa }

constructor TJeraDARemessa.Create(Banco: TJeraDABancos);
begin
  inherited Create;
  FBanco := Banco;
  FBlocoC := TDADIClasseItemsBase<IJeraDABlocoC>.Create;
  FBlocoD := TDADIClasseItemsBase<IJeraDABlocoD>.Create;
  FBlocoE := TDADIClasseItemsBase<IJeraDABlocoE>.Create;
  case Banco of
    daNenhum:
      raise Exception.Create('Nenhum banco informado!');
    daBradesco:
      begin
        FBlocoA := TJeraDABradescoBlocoA.Create;
        FBlocoC.Classe := TJeraDABradescoBlocoC;
        FBlocoD.Classe := TJeraDABradescoBlocoD;
        FBlocoE.Classe := TJeraDABradescoBlocoE;
        FBlocoZ := TJeraDABradescoBlocoZ.Create;
      end;
    daCEF:
      begin
        FBlocoA := TJeraDACEFBlocoA.Create;
        FBlocoC.Classe := TJeraDACEFBlocoC;
        FBlocoD.Classe := TJeraDACEFBlocoD;
        FBlocoE.Classe := TJeraDACEFBlocoE;
        FBlocoZ := TJeraDACEFBlocoZ.Create;
      end;
    daItau:
      begin
        FBlocoA := TJeraDAItauBlocoA.Create;
        FBlocoC.Classe := TJeraDAItauBlocoC;
        FBlocoD.Classe := TJeraDAItauBlocoD;
        FBlocoE.Classe := TJeraDAItauBlocoE;
        FBlocoZ := TJeraDAItauBlocoZ.Create;
      end;
  end;
  FBlocoA.CodigoRemessa := rrRemessa;
end;

destructor TJeraDARemessa.Destroy;
begin
  FreeAndNil(FBlocoE);
  FreeAndNil(FBlocoD);
  FreeAndNil(FBlocoC);
  inherited;
end;

procedure TJeraDARemessa.Processar;
var
  OStr: TStringList;
  I: Integer;
  SDir: string;
begin
  if not SelectDirectory('Selecione uma pasta para salvar o arquivo','', SDir, [sdNewFolder, sdNewUI]) then
    Exit;

  FArquivoRemessa := IncludeTrailingPathDelimiter(SDir) +
                     FormatDateTime('yyyymmyy', BlocoA.DataGeracao) +
                     IntToStr(BlocoA.NSA) + '.rem';
  OStr := TStringList.Create;
  try
    // Bloco A
    OStr.Add(BlocoA.MontarLinha);

    // Bloco C
    for I := 0 to BlocoC.Count-1 do
    begin
      if FBanco = daCEF then
        BlocoC.Items[I].Brancos1 := ACBrUtil.Strings.PadLeft((1 + (I+1)).ToString, 6, '0');
      OStr.Add(BlocoC.Items[I].MontarLinha);
    end;

//    // Bloco D
//    for I := 0 to BlocoD.Count-1 do
//      OStr.Add(BlocoD.Items[I].MontarLinha);

    // Bloco E
    for I := 0 to BlocoE.Count-1 do
    begin
      if FBanco = daCEF then
      begin
        BlocoE.Items[I].Complemento := ACBrUtil.Strings.PadLeft((I+1).ToString, 6, '0');
        BlocoE.Items[I].Brancos2 := ACBrUtil.Strings.PadLeft((1 + BlocoC.Count + (I+1)).ToString, 6, '0');
      end;
      OStr.Add(BlocoE.Items[I].MontarLinha);
    end;

    // Qtde total de registros no arquivo
    BlocoZ.TotalRegistrosArquivo := 1 + // BlocoA
                                    BlocoC.Count +
                                    BlocoD.Count +
                                    BlocoE.Count +
                                    1; // BlocoZ

    // Valor total
    BlocoZ.ValorTotalRegistros := 0;
    for I := 0 to BlocoE.Count-1 do
      BlocoZ.ValorTotalRegistros := BlocoZ.ValorTotalRegistros +
                                    BlocoE.Items[I].ValorDebito;

    // Bloco Z
    if FBanco = daCEF then
      BlocoZ.Brancos1 := ACBrUtil.Strings.PadLeft(BlocoZ.TotalRegistrosArquivo.ToString, 6, '0');
    OStr.Add(BlocoZ.MontarLinha);
//    if FBlocoA.CodigoBanco = '104' then
//    begin
//      // CEF possui n�mero sequencial em seus registros
//      __IncluirSequencial(OStr, 6, 144);
//    end;
    OStr.SaveToFile(FArquivoRemessa, TEncoding.ANSI);
  finally
    FreeAndNil(OStr);
  end;
end;

function TJeraDARemessa.GetArquivoRemessa: string;
begin
  Result := FArquivoRemessa;
end;

function TJeraDARemessa.GetBlocoA: IJeraDABlocoA;
begin
  Result := FBlocoA;
end;

function TJeraDARemessa.GetBlocoC: TDADIClasseItemsBase<IJeraDABlocoC>;
begin
  Result := FBlocoC;
end;

function TJeraDARemessa.GetBlocoD: TDADIClasseItemsBase<IJeraDABlocoD>;
begin
  Result := FBlocoD;
end;

function TJeraDARemessa.GetBlocoE: TDADIClasseItemsBase<IJeraDABlocoE>;
begin
  Result := FBlocoE;
end;

function TJeraDARemessa.GetBlocoZ: IJeraDABlocoZ;
begin
  Result := FBlocoz;
end;

procedure TJeraDARemessa.SetBlocoA(const Value: IJeraDABlocoA);
begin
  FBlocoA := Value;
end;

procedure TJeraDARemessa.SetBlocoC(
  const Value: TDADIClasseItemsBase<IJeraDABlocoC>);
begin
  FBlocoC := Value;
end;

procedure TJeraDARemessa.SetBlocoD(
  const Value: TDADIClasseItemsBase<IJeraDABlocoD>);
begin
  FBlocoD := Value;
end;

procedure TJeraDARemessa.SetBlocoE(
  const Value: TDADIClasseItemsBase<IJeraDABlocoE>);
begin
  FBlocoE := Value;
end;

procedure TJeraDARemessa.SetBlocoZ(const Value: IJeraDABlocoZ);
begin
  FBlocoZ := Value;
end;

procedure TJeraDARemessa.__IncluirSequencial(var AStr: TStringList; const ADigitos, AInicio: byte);
var
  I: Integer;
  SLinha: string;
begin
  for I := 0 to AStr.Count-1 do
  begin
    SLinha := AStr[I];
    Delete(SLinha, AInicio, ADigitos);
    Insert(ACBrUtil.Strings.PadLeft((I+1).ToString, ADigitos, '0'), SLinha, AInicio);
    AStr[I] := SLinha;
  end;
end;

{ TJeraDARetorno }

constructor TJeraDARetorno.Create(Banco: TJeraDABancos);
begin
  inherited Create;
  FBanco := Banco;
  FBlocoB := TDADIClasseItemsBase<IJeraDABlocoB>.Create;
  FBlocoC := TDADIClasseItemsBase<IJeraDABlocoC>.Create;
  FBlocoF := TDADIClasseItemsBase<IJeraDABlocoF>.Create;
  FBlocoH := TDADIClasseItemsBase<IJeraDABlocoH>.Create;
  case Banco of
    daNenhum:
      raise Exception.Create('Nenhum banco informado!');
    daBradesco:
      begin
        FBlocoA := TJeraDABradescoBlocoA.Create;
        FBlocoB.Classe := TJeraDABradescoBlocoB;
        FBlocoC.Classe := TJeraDABradescoBlocoC;
        FBlocoF.Classe := TJeraDABradescoBlocoF;
        FBlocoH.Classe := TJeraDABradescoBlocoH;
        FBlocoZ := TJeraDABradescoBlocoZ.Create;
      end;
    daCEF:
      begin
        FBlocoA := TJeraDACEFBlocoA.Create;
        FBlocoB.Classe := TJeraDACEFBlocoB;
        FBlocoC.Classe := TJeraDACEFBlocoC;
        FBlocoF.Classe := TJeraDACEFBlocoF;
        FBlocoH.Classe := TJeraDACEFBlocoH;
        FBlocoZ := TJeraDACEFBlocoZ.Create;
      end;
    daItau:
      begin
        FBlocoA := TJeraDAItauBlocoA.Create;
        FBlocoB.Classe := TJeraDAItauBlocoB;
        FBlocoC.Classe := TJeraDAItauBlocoC;
        FBlocoF.Classe := TJeraDAItauBlocoF;
        FBlocoH.Classe := TJeraDAItauBlocoH;
        FBlocoZ := TJeraDAItauBlocoZ.Create;
      end;
  end;
end;

destructor TJeraDARetorno.Destroy;
begin
  FreeAndNil(FBlocoH);
  FreeAndNil(FBlocoF);
  FreeAndNil(FBlocoC);
  FreeAndNil(FBlocoB);
  inherited;
end;

function TJeraDARetorno.GetBlocoA: IJeraDABlocoA;
begin
  Result := FBlocoA;
end;

function TJeraDARetorno.GetBlocoB: TDADIClasseItemsBase<IJeraDABlocoB>;
begin
  Result := FBlocoB;
end;

function TJeraDARetorno.GetBlocoC: TDADIClasseItemsBase<IJeraDABlocoC>;
begin
  Result := FBlocoC;
end;

function TJeraDARetorno.GetBlocoF: TDADIClasseItemsBase<IJeraDABlocoF>;
begin
  Result := FBlocoF;
end;

function TJeraDARetorno.GetBlocoH: TDADIClasseItemsBase<IJeraDABlocoH>;
begin
  Result := FBlocoH;
end;

function TJeraDARetorno.GetBlocoZ: IJeraDABlocoZ;
begin
  Result := BlocoZ;
end;

procedure TJeraDARetorno.Processar(AFileName: string);
var
  OStr: TStringList;
  I: Integer;
begin
  OStr := TStringList.Create;
  try
    OStr.LoadFromFile(AFileName);
    FBlocoA.CarregarLinha(OStr[0]);
    FBlocoZ.CarregarLinha(OStr[OStr.Count-1]);
    for I := 1 to OStr.Count-2 do
    begin
      if Copy(OStr[I], 1, 1) = 'B' then
        FBlocoB.New.CarregarLinha(OStr[i]);
      if Copy(OStr[I], 1, 1) = 'C' then
        FBlocoC.New.CarregarLinha(OStr[i]);
      if Copy(OStr[I], 1, 1) = 'F' then
        FBlocoF.New.CarregarLinha(OStr[i]);
      if Copy(OStr[I], 1, 1) = 'H' then
        FBlocoH.New.CarregarLinha(OStr[i]);
    end;
  finally
    FreeAndNil(OStr);
  end;
end;

procedure TJeraDARetorno.SetBlocoA(const Value: IJeraDABlocoA);
begin
  FBlocoA := Value;
end;

procedure TJeraDARetorno.SetBlocoB(const Value: TDADIClasseItemsBase<IJeraDABlocoB>);
begin
  FBlocoB := Value;
end;

procedure TJeraDARetorno.SetBlocoC(const Value: TDADIClasseItemsBase<IJeraDABlocoC>);
begin
  FBlocoC := Value;
end;

procedure TJeraDARetorno.SetBlocoF(const Value: TDADIClasseItemsBase<IJeraDABlocoF>);
begin
  FBlocoF := Value;
end;

procedure TJeraDARetorno.SetBlocoH(const Value: TDADIClasseItemsBase<IJeraDABlocoH>);
begin
  FBlocoH := Value;
end;

procedure TJeraDARetorno.SetBlocoZ(const Value: IJeraDABlocoZ);
begin
  FBlocoZ := Value;
end;

//{ TDAOcorrenciaBlocoCH }
//
//function TDAOcorrenciaBlocoCH.ToString: string;
//begin
//  case Self of
//    obcIdentificacaoDoCLienteNaoLocalizada:
//      Result := 'Identifica��o do cliente n�o localizada';
//    obcRestricaoDeCadastramentoPelaEmpresa:
//      Result := 'Restricao de Cadastramento Pela Empresa';
//    obcClienteCadastradoEmOutroBancoComDataPosterior:
//      Result := 'Cliente cadastrado em outro banco com data posterior';
//    obcClienteDesativadoNoCadastroDaEmpresa:
//      Result := 'Cliente desativado no cadastro da empresa';
//    obcOperadoraInvalida:
//      Result := 'Operadora Inv�lida';
//  end;
//end;

//{ TDACodigoRetornoDS }
//
//function TDACodigoRetornoDS.ToString: string;
//begin
//  case Self of
//    ccr00DebitoEfetuado:
//      Result := 'D�bito efetuado';
//    ccr01DebitonaoEfetuadoInsuficienciadeFundos:
//      Result := 'D�bito n�o Efetuado - Insuficiencia de Fundos';
//    ccr02DebitonaoEfetuadoContaNaoCadastrada:
//      Result := 'D�bito n�o Efetuado - Conta N�o Cadastrada';
//    ccr04DebitonaoEfetuadoOutrasRestricoes:
//      Result := 'D�bito n�o Efetuado - Outras Restri��es';
//    ccr05DebitonaoEfetuadoValorExcedeLimiteAprovado:
//      Result := 'D�bito n�o Efetuado - Valor Excede Limite Aprovado';
//    ccr10DebitonaoEfetuadoAgenciaRegimeEncerramento:
//      Result := 'D�bito n�o Efetuado - Agencia Regime Encerramento';
//    ccr12DebitonaoEfetuadoValorInvalido:
//      Result := 'D�bito n�o Efetuado - Valor Inv�lido';
//    ccr13DebitonaoEfetuadoDataLancamentoInvalida:
//      Result := 'D�bito n�o Efetuado - Data Lan�amento Inv�lida';
//    ccr14DebitonaoEfetuadoAgenciaInvalida:
//      Result := 'D�bito n�o Efetuado - Agencia Inv�lida';
//    ccr15DebitonaoEfetuadoContaInvalida:
//      Result := 'D�bito n�o Efetuado - Conta Inv�lida';
//    ccr18DebitonaoEfetuadoDataInferiorProcessamento:
//      Result := 'D�bito n�o Efetuado - Data Inferior Processamento';
//    ccr19DebitonaoEfetuadoContaNaoPertenceCNPJCPF:
//      Result := 'D�bito n�o Efetuado - Conta N�o Pertence CNPJ/CPF';
//    ccr20DebitonaoEfetuadoContaConjuntaNaoSolidaria:
//      Result := 'D�bito n�o Efetuado - Conta Conjunta N�o Solid�ria';
//    ccr30DebitonaoEfetuadoSemContratoDebitoAutomatico:
//      Result := 'D�bito n�o Efetuado - Sem Contrato D�bito Autom�tico';
//    ccr31DebitoEfetuadoDataDiferenteFeriadoPracaDevito:
//      Result := 'D�bito Efetuado - Data Diferente Feriado Pra�a Debito';
//    ccr47DebitoNaoEfetuadoValorDebitoAcimaLimite:
//      Result := 'D�bito N�o Efetuado - Valor D�bito Acima Limite';
//    ccr48DebitoNaoEfetuadoLimiteDiarioDebitoUltrapassado:
//      Result := 'Debito N�o Efetuado - Limite Di�rio D�bito Ultrapassado';
//    ccr49DebitoNaoEfetuadoCnpjCpfDebitadoInvalido:
//      Result := 'D�bito N�o Efetuado - Cnpj/Cpf Debitado Inv�lido';
//    ccr50DebitoNaoEfetuadoCnpjCpfNaoPertenceContaDbitada:
//      Result := 'D�bito N�o Efetuado - Cnpj/Cpf N�o Pertence Conta Debitada';
//    ccr96ManutencaoCadastro:
//      Result := 'Manuten��o Cadastro';
//    ccr97CancelamentoNaoEncontrado:
//      Result := 'Cancelamento N�o Encontrado';
//    ccr98CancelamentoMaoEfetuadoForaTempoHabil:
//      Result := 'Cancelamento N�o Efetuado Fora Tempo H�bil';
//    ccr99CancelamentoCanceladoConformeSolicitacao:
//      Result := 'Cancelamento Cancelado Conforme Solicita��o';
//    ccrAQTipoQtdeMoedaInvalida:
//      Result := 'Tipo Qtde Moeda Inv�lida';
//    ccrIDValorMoraInvallido:
//      Result := 'Valor Mora Inv�lido';
//    ccrBDConfirmacaoAgendamento:
//      Result := 'Confirma��o Agendamento';
//    ccrPEDebitoPendenteAutorizacao:
//      Result := 'D�bito Pendente de Autoriza��o';
//    ccrNADebitoNaoAutorizado:
//      Result := 'Debito N�o Autorizado';
//    ccrATDebitoAutorizado:
//      Result := 'D�bito Autorizado';
//    ccrRCDebitoRecusao:
//      Result := 'D�bito Recusao';
//  else
//    Result := GetEnumName(TypeInfo(TDACodigoRetorno), Ord(Self));
//  end;
//end;

{ TDACodigoMovimentoBlocoBHelper }

function TDACodigoMovimentoBlocoBHelper.ToString: string;
begin
  case Self of
    cmbbExclusao:
      Result := 'Exlus�o';
    cmbbInclusao:
      Result := 'Inclus�o';
    cmddOutros:
      Result := 'Outros';
  end;
end;

end.
