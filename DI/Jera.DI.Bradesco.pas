{******************************************************************************}
{ Projeto: TJeraDIBradesco                                                     }
{                                                                              }
{ Fun��o: Fazer a leitura de arquivo magn�tico recebidos de bancos, referente  }
{         Dep�sitos Identificados - Banco Bradesco                             }
{                                                                              }
{         Essas rotinas n�o fazem a gera��o do mesmo, pois isso n�o � previsto }
{         nessa modalidade de servi�o prestado pelos bancos.                   }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{******************************************************************************}

{******************************************************************************}
{ Direitos Autorais Reservados � 2016 - J�ter Rabelo Ferreira                  }
{ Contato: jeter.rabelo@jerasoft.com.br                                        }
{******************************************************************************}
unit Jera.DI.Bradesco;

interface

uses
  System.SysUtils, System.Classes, Jera.DI, Jera.DADI.Util;

type
  TJeraDIHBradesco = class(TInterfacedObject, IFinanceitoDIH)
  private
    [DadosDefinicoes(fdatString, 104, 3)]
    FNumeroBanco: string;

    [DadosDefinicoes(fdatString, 119, 5)]
    FAgencia: string;

    [DadosDefinicoes(fdatString, 129, 10)]
    FNumContaCorrente: string;

    [DadosDefinicoes(fdatString, 139, 2)]
    FNumContaCorrenteDigito: string;

    [DadosDefinicoes(fdatInteger, 234, 7)]
    FNumeroSequencialArq: Integer;

    function GetNumeroBanco: string;
    procedure SetNumeroBanco(const Value: string);
    function GetAgencia: string;
    procedure SetAgencia(const Value: string);
    function GetNumContaCorrente: string;
    procedure SetNumContaCorrente(const Value: string);
    function GetNumContaCorrenteDigito: string;
    procedure SetNumContaCorrenteDigito(const Value: string);
    function GetNumeroSequencialArq: Integer;
    procedure SetNumeroSequencialArq(const Value: Integer);
    function GetClasse: TClass;
  public
    procedure CarregarLinha(const Value: string);
    property CLasse: TClass read GetClasse;
    property NumeroBanco: string read GetNumeroBanco write SetNumeroBanco;
    property Agencia: string read GetAgencia write SetAgencia;
    property NumContaCorrente: string read GetNumContaCorrente write SetNumContaCorrente;
    property NumContaCorrenteDigito: string read GetNumContaCorrenteDigito write SetNumContaCorrenteDigito;
    property NumeroSequencialArq: Integer read GetNumeroSequencialArq write SetNumeroSequencialArq;
  end;

  TJeraDIBradescoItens = class(TInterfacedObject, IJeraDII)
  private
    [DadosDefinicoes(fdatData, 2, 8, 'DDMMYYYY')]
    FDataDeposito: TDate;

    [DadosDefinicoes(fdatString, 10, 5)]
    FAgenciaAcolhedora: string;

    [DadosDefinicoes(fdatString, 15, 1)]
    FAgenciaAcolhedoraDigito: string;

    [DadosDefinicoes(fdatString, 40, 22)]
    FIdentificador: string;

    [DadosDefinicoes(fdatMoney, 92, 15)]
    FValorTotal: Double;

    [DadosDefinicoes(fdatInteger, 107, 8)]
    FNumeroDocumento: Integer;

    [DadosDefinicoes(fdatInteger, 234, 7)]
    FNumeroSequencialArq: Integer;

    function GetDataDeposito: TDate;
    procedure SetDataDeposito(const Value: TDate);
    function GetAgenciaAcolhedora: string;
    procedure SetAgenciaAcolhedora(const Value: string);
    function GetAgenciaAcolhedoraDigito: string;
    procedure SetAgenciaAcolhedoraDigito(const Value: string);
    function GetIdentificador: string;
    procedure SetIdentificador(const Value: string);
    function GetValorTotal: Double;
    procedure SetValorTotal(const Value: Double);
    function GetNumeroDocumento: Integer;
    procedure SetNumeroDocumento(const Value: Integer);
    function GetNumeroSequencialArq: Integer;
    procedure SetNumeroSequencialArq(const Value: Integer);
    function GetClasse: TClass;
  public
    procedure CarregarLinha(const Value: string);
    property CLasse: TClass read GetClasse;
    property DataDeposito: TDate read GetDataDeposito write SetDataDeposito;
    property AgenciaAcolhedora: string read GetAgenciaAcolhedora write SetAgenciaAcolhedora;
    property AgenciaAcolhedoraDigito: string read GetAgenciaAcolhedoraDigito write SetAgenciaAcolhedoraDigito;
    property Identificador: string read GetIdentificador write SetIdentificador;
    property ValorTotal: Double read GetValorTotal write SetValorTotal;
    property NumeroDocumento: Integer read GetNumeroDocumento write SetNumeroDocumento;
    property NumeroSequencialArq: Integer read GetNumeroSequencialArq write SetNumeroSequencialArq;
  end;

  TJeraDIBradescoTrailler = class(TInterfacedObject, IFinanceitoDIT)
  private
    [DadosDefinicoes(fdatInteger, 68, 7)]
    FQtde: Integer;

    [DadosDefinicoes(fdatMoney, 75, 15)]
    FValor: Double;

    [DadosDefinicoes(fdatInteger, 234, 7)]
    FNumeroSequencialArq: Integer;

    function GetQtde: Integer;
    procedure SetQtde(const Value: Integer);
    function GetValor: Currency;
    procedure SetValor(const Value: Currency);
    function GetNumeroSequencialArq: Integer;
    procedure SetNumeroSequencialArq(const Value: Integer);
    function GetClasse: TClass;
  public
    procedure CarregarLinha(const Value: string);
    property CLasse: TClass read GetClasse;
    property Qtde: Integer read GetQtde write SetQtde;
    property Valor: Currency read GetValor write SetValor;
    property NumeroSequencialArq: Integer read GetNumeroSequencialArq write SetNumeroSequencialArq;
  end;

implementation

uses
  StrUtils;

{ TJeraDIBradescoItens }

procedure TJeraDIBradescoItens.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

function TJeraDIBradescoItens.GetAgenciaAcolhedora: string;
begin
  Result := FAgenciaAcolhedora;
end;

function TJeraDIBradescoItens.GetAgenciaAcolhedoraDigito: string;
begin
  Result := FAgenciaAcolhedoraDigito;
end;

function TJeraDIBradescoItens.GetClasse: TClass;
begin
  Result := Self.ClassType;
end;

function TJeraDIBradescoItens.GetDataDeposito: TDate;
begin
  Result := FDataDeposito;
end;

function TJeraDIBradescoItens.GetIdentificador: string;
begin
  Result := FIdentificador;
end;

function TJeraDIBradescoItens.GetNumeroDocumento: Integer;
begin
  Result := FNumeroDocumento;
end;

function TJeraDIBradescoItens.GetNumeroSequencialArq: Integer;
begin
  Result := FNumeroSequencialArq;
end;

function TJeraDIBradescoItens.GetValorTotal: Double;
begin
  Result := FValorTotal;
end;

procedure TJeraDIBradescoItens.SetAgenciaAcolhedora(const Value: string);
begin
  FAgenciaAcolhedora := Value;
end;

procedure TJeraDIBradescoItens.SetAgenciaAcolhedoraDigito(
  const Value: string);
begin
  FAgenciaAcolhedoraDigito := Value;
end;

procedure TJeraDIBradescoItens.SetDataDeposito(const Value: TDate);
begin
  FDataDeposito := Value;
end;

procedure TJeraDIBradescoItens.SetIdentificador(const Value: string);
var
  I: Integer;
  SId: string;
begin
  FIdentificador := Value;

  // Retirar os zeros a esquerda
  SId := Copy(FIdentificador, 1, Length(FIdentificador)-2);
  FIdentificador := IntToStr(StrToIntDef(SId, 0)) + '-' + RightStr(Value, 1);
end;

procedure TJeraDIBradescoItens.SetNumeroDocumento(const Value: Integer);
begin
  FNumeroDocumento := Value;
end;

procedure TJeraDIBradescoItens.SetNumeroSequencialArq(
  const Value: Integer);
begin
  FNumeroSequencialArq := Value;
end;

procedure TJeraDIBradescoItens.SetValorTotal(const Value: Double);
begin
  FValorTotal := Value;
end;

{ TJeraDIBradescoTrailler }

procedure TJeraDIBradescoTrailler.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

function TJeraDIBradescoTrailler.GetClasse: TClass;
begin
  Result := Self.ClassType;
end;

function TJeraDIBradescoTrailler.GetNumeroSequencialArq: Integer;
begin
  Result := FNumeroSequencialArq;
end;

function TJeraDIBradescoTrailler.GetQtde: Integer;
begin
  Result := FQtde;
end;

function TJeraDIBradescoTrailler.GetValor: Currency;
begin
  Result := FValor;
end;

procedure TJeraDIBradescoTrailler.SetNumeroSequencialArq(
  const Value: Integer);
begin
  FNumeroSequencialArq := Value;
end;

procedure TJeraDIBradescoTrailler.SetQtde(const Value: Integer);
begin
  FQtde := Value;
end;

procedure TJeraDIBradescoTrailler.SetValor(const Value: Currency);
begin
  FValor := Value;
end;

{ TJeraDIHBradesco }

procedure TJeraDIHBradesco.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

function TJeraDIHBradesco.GetAgencia: string;
begin
  Result := FAgencia;
end;

function TJeraDIHBradesco.GetClasse: TClass;
begin
  Result := Self.ClassType;
end;

function TJeraDIHBradesco.GetNumContaCorrente: string;
begin
  Result := FNumContaCorrente;
end;

function TJeraDIHBradesco.GetNumContaCorrenteDigito: string;
begin
  Result := FNumContaCorrenteDigito
end;

function TJeraDIHBradesco.GetNumeroBanco: string;
begin
  Result := FNumeroBanco;
end;

function TJeraDIHBradesco.GetNumeroSequencialArq: Integer;
begin
  Result := FNumeroSequencialArq;
end;

procedure TJeraDIHBradesco.SetAgencia(const Value: string);
begin
  FAgencia := Value;
end;

procedure TJeraDIHBradesco.SetNumContaCorrente(const Value: string);
begin
  FNumContaCorrente := Value;
end;

procedure TJeraDIHBradesco.SetNumContaCorrenteDigito(const Value: string);
begin
  FNumContaCorrenteDigito := Value;
end;

procedure TJeraDIHBradesco.SetNumeroBanco(const Value: string);
begin
  FNumeroBanco := Value;
end;

procedure TJeraDIHBradesco.SetNumeroSequencialArq(const Value: Integer);
begin
  FNumeroSequencialArq := Value;
end;

end.
