{******************************************************************************}
{ Projeto: TJeraDIDAUtil                                                       }
{                                                                              }
{ Fun��o: Utilidades para a utiliza��o nas classes de D�bito Autorizado e de   }
{         Dep�sitos Identificados.                                             }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{******************************************************************************}

{******************************************************************************}
{ Direitos Autorais Reservados � 2016 - J�ter Rabelo Ferreira                  }
{ Contato: jeter.rabelo@jerasoft.com.br                                        }
{******************************************************************************}
unit Jera.DADI.Util;

interface
uses
  System.SysUtils, System.Classes, System.Rtti, System.Generics.Collections;

type
  TDADITipoDado = (fdatString, fdatInteger, fdatMoney, fdatData, fdatHora, fdatDataHora);
  TDADIIdentificacao = (iCNPJ, iCPF, IOutros);
  TDADIPlusMinus = (pmPlus,pmMinus);

  DadosDefinicoes = class(TCustomAttribute)
  private
    FTipoDado: TDADITipoDado;
    FTamanho: SmallInt;
    FStartPos: SMallInt;
    FDisplayFormat: string;
    FUtilizarPipe: Boolean;
  public
    constructor Create(ATipoDados: TDADITipoDado;
                       AStartPos: SmallInt;
                       ATamanho: SmallInt;
                       ADisplayFormat: string = ''); overload;
    constructor Create(ATipoDados: TDADITipoDado;
                       AStartPos: SmallInt;
                       ATamanho: SmallInt;
                       AUtilizarPipe: Boolean;
                       ADisplayFormat: string = ''); overload;
    constructor Create(ATipoDados: TDADITipoDado;
                       APosicao: SmallInt;
                       ADisplayFormat: string = ''); overload;
    property TipoDado: TDADITipoDado read FTipoDado write FTipoDado;
    property Tamanho: SmallInt read FTamanho write FTamanho;
    property StartPos: SMallInt read FStartPos write FStartPos;
    property DisplayFormat: string read FDisplayFormat write FDisplayFormat;
    property UtilizarPipe: Boolean read FUtilizarPipe write FUtilizarPipe;
  end;

  Enumerado = class(TCustomAttribute)
  private
    FValores: TList<string>;
  public
    constructor Create(AValores: string);
    destructor Destroy; override;
    property Valores: TList<string> read FValores write FValores;
  end;

  ValorConstante = class(TCustomAttribute)
  private
    FTipoDado: TDADITipoDado;
    FValor: Variant;
  public
    constructor Create(ATipoDado: TDADITipoDado;
                       AValor: Variant);
    property TipoDado: TDADITipoDado read FTipoDado write FTipoDado;
    property Valor: Variant read FValor write FValor;
  end;

  TJeraDADIUtils = class
  public
    class function GetAttribute<T: TCustomAttribute>(const Obj: TRttiObject): T; overload;
    class function PropertyFor(const RttiF: TRttiField): TRttiProperty;
    class function GetDadosDefinicoes(AClass: TClass;
                                      AProperty: string;
                                      ALinha: string): TValue;
    class function MontarLinhaFromObj(AObj: TObject): string;
    class procedure CarregarLinhaToObj(const ALinha: string;
                                       AObj: TObject); overload;
  end;

  TDADIClasseItemsBase<T> = class
  private
    FLista: TList<T>;
    FClazz: TClass;
//    FItemIsInterface: Boolean;
    function GetItem(Index: integer): T;
    function CreateObj(Classe: TClass): TObject;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Clear;
    function Count: Integer;
    procedure Delete(Index: Integer);
    function New: T;
    function Add(Obj: T): Integer;
    property Items[Index: Integer]: T read GetItem;
    property Classe: TClass read FClazz write FClazz;
//    property ItemIsInterface: Boolean read FItemIsInterface write FItemIsInterface;
  end;

implementation

uses
  ACBrUtil.DateTime,
  ACBrUtil.Strings,
  System.TypInfo,
  System.StrUtils;

{ DadosDefinicoes }

constructor DadosDefinicoes.Create(ATipoDados: TDADITipoDado;
                                   AStartPos: SmallInt;
                                   ATamanho: SmallInt;
                                   ADisplayFormat: string);
begin
  TipoDado := ATipoDados;
  Tamanho := ATamanho;
  StartPos := AStartPos;
  DisplayFormat := ADisplayFormat;
  UtilizarPipe := False;
end;

constructor DadosDefinicoes.Create(ATipoDados: TDADITipoDado;
                                   AStartPos, ATamanho: SmallInt;
                                   AUtilizarPipe: Boolean;
                                   ADisplayFormat: string);
begin
  TipoDado := ATipoDados;
  Tamanho := ATamanho;
  StartPos := AStartPos;
  UtilizarPipe := AUtilizarPipe;
  DisplayFormat := ADisplayFormat;
end;

constructor DadosDefinicoes.Create(ATipoDados: TDADITipoDado;
                                   APosicao: SmallInt;
                                   ADisplayFormat: string);
begin
  TipoDado := ATipoDados;
  StartPos := APosicao;
  DisplayFormat := ADisplayFormat;
  Tamanho := 0;
  UtilizarPipe := True;
end;

{ Enumerado }

constructor Enumerado.Create(AValores: string);
var
  ALista: TStringList;
  I: Integer;
begin
  if (Trim(AValores) = '') then
    raise Exception.Create('Nenhum enumerado informado!');

  FValores := TList<string>.Create;
  ALista := TStringList.Create;
  try
    ALista.Duplicates := dupError;
    ExtractStrings([',', ';'], [' '], PChar(AValores), ALista);
    for I := 0 to ALista.Count - 1 do
    begin
      if Trim(ALista[I]) <> '@' then
        FValores.Add(Trim(ALista[I]))
      else
        FValores.Add('');
    end;
  finally
    FreeAndNil(ALista);
  end;
end;

destructor Enumerado.Destroy;
begin
  FreeAndNil(FValores);
  inherited;
end;

{ TJeraDADIUtils }

class procedure TJeraDADIUtils.CarregarLinhaToObj(const ALinha: string;
                                                  AObj: TObject);
var
  RttiC: TRttiContext;
  RttiT: TRttiType;
  RttiF: TRttiField;
  RttiP: TRttiProperty;
  RttiA: TCustomAttribute;
  VValue: TValue;
  SField, SProperty: string;
begin
  RttiC := TRttiContext.Create;
  try
    try
      RttiT := RttiC.GetType(AObj.ClassType);
      for RttiF in RttiT.GetFields do
      begin
        RttiA := TJeraDADIUtils.GetAttribute<DadosDefinicoes>(RttiF);
        if Assigned(RttiA) then
        begin
          SField := RttiF.Name;
          SProperty := Copy(RttiF.Name, 2, Length(RttiF.Name));
          VValue  := TJeraDADIUtils.GetDadosDefinicoes(AObj.ClassType,
                                                       SProperty,
                                                       ALinha);
          RttiP := TJeraDADIUtils.PropertyFor(RttiF);
          if RttiP.IsWritable then
            RttiP.SetValue(AObj, VValue);
        end;
      end;
    finally
      RttiC.Free;
    end;
  Except
    on E: Exception do
    begin
      raise Exception.Create('Erro ao carregar registro' + sLineBreak +
                             'Field: ' + SField + sLineBreak +
                             'Property: ' + SProperty);
    end;
  end;
end;

class function TJeraDADIUtils.GetAttribute<T>(const Obj: TRttiObject): T;
var
  Attr: TCustomAttribute;
begin
  Result := nil;
  for Attr in Obj.GetAttributes do
  begin
    if Attr.ClassType.InheritsFrom(T) then
      Exit(T(Attr));
  end;
end;

class function TJeraDADIUtils.GetDadosDefinicoes(AClass: TClass;
                                                 AProperty: string;
                                                 ALinha: string): TValue;
var
  RttiC: TRttiContext;
  RttiT: TRttiType;
  RttiF: TRttiField;
  RttiA: TCustomAttribute;
  function _GetString: TValue;
  var
    I: Integer;
    SCampo: string;
    RttiAE: TCustomAttribute;
  begin
    if (not DadosDefinicoes(Rttia).UtilizarPipe and
       (DadosDefinicoes(Rttia).StartPos <= 0) and
       (DadosDefinicoes(Rttia).Tamanho <= 0))  then
      Exit('');

    if DadosDefinicoes(Rttia).UtilizarPipe then
      SCampo := ALinha.Split(['|'])[DadosDefinicoes(Rttia).StartPos]
    else
      SCampo := Trim(Copy(ALinha,
                        DadosDefinicoes(Rttia).StartPos,
                        DadosDefinicoes(Rttia).Tamanho));
    if RttiF.FieldType.TypeKind <> tkEnumeration then
    begin
      Result := SCampo;
      Exit;
    end
    else
    begin
      RttiAE := TJeraDADIUtils.GetAttribute<Enumerado>(RttiF);
      if Assigned(RttiAE) then
      begin
        for I := 0 to Enumerado(RttiAE).Valores.Count-1 do
        begin
          if LowerCase(SCampo) = LowerCase(Enumerado(RttiAE).Valores[I]) then
          begin
            Result := TValue.FromOrdinal(RttiF.FieldType.Handle, I);
            Exit;
          end;
        end;
      end;
    end;
  end;

  function _GetData: TValue;
  var
    SCampo: string;
    SFormat: string;
  begin
    if (not DadosDefinicoes(Rttia).UtilizarPipe and
       (DadosDefinicoes(Rttia).StartPos <= 0) and
       (DadosDefinicoes(Rttia).Tamanho <= 0))  then
      Exit(0);

    if DadosDefinicoes(Rttia).UtilizarPipe then
      SCampo := ALinha.Split(['|'])[DadosDefinicoes(Rttia).StartPos]
    else
      SCampo := Copy(ALinha,
                     DadosDefinicoes(Rttia).StartPos,
                     DadosDefinicoes(Rttia).Tamanho);
    SFormat := UpperCase(DadosDefinicoes(Rttia).DisplayFormat);
    if SFormat = '' then
      SFormat := 'DDMMYYYY';
    if UpperCase(SFormat) = 'DDMMYYYY' then
      SCampo := Copy(SCampo, 1, 2) + '/' +
                Copy(SCampo, 3, 2) + '/' +
                Copy(SCampo, 5, 4)
    else if UpperCase(SFormat) = 'YYMMDD' then
    begin
      SCampo := Copy(SCampo, 1, 2) + '/' +
                Copy(SCampo, 3, 2) + '/' +
                Copy(SCampo, 5, 2)
    end
    else if UpperCase(SFormat) = 'YYYYMMDD' then
      SCampo := Copy(SCampo, 1, 4) + '/' +
                Copy(SCampo, 5, 2) + '/' +
                Copy(SCampo, 7, 2);
    Result := StringToDateTimeDef(SCampo, 0, SFormat);
  end;

  function _GetTime: TValue;
  var
    SCampo: string;
    SFormat: string;
  begin
    if (not DadosDefinicoes(Rttia).UtilizarPipe and
       (DadosDefinicoes(Rttia).StartPos <= 0) and
       (DadosDefinicoes(Rttia).Tamanho <= 0))  then
      Exit(0);

    if DadosDefinicoes(Rttia).UtilizarPipe then
      SCampo := ALinha.Split(['|'])[DadosDefinicoes(Rttia).StartPos]
    else
      SCampo := Copy(ALinha,
                     DadosDefinicoes(Rttia).StartPos,
                     DadosDefinicoes(Rttia).Tamanho);
    SFormat := UpperCase(DadosDefinicoes(Rttia).DisplayFormat);
    if SFormat <> 'HHMMSS' then
      raise Exception.Create('Formata��o de campo Hora n�o cadastrado!');

    Result := EncodeTime(Copy(SCampo, 1, 2).ToInteger,
                         Copy(SCampo, 3, 2).ToInteger,
                         Copy(SCampo, 5, 2).ToInteger, 0);
  end;

  function _GetDataHora: TValue;
  var
    SCampo: string;
    SFormat: string;
  begin
    if (not DadosDefinicoes(Rttia).UtilizarPipe and
       (DadosDefinicoes(Rttia).StartPos <= 0) and
       (DadosDefinicoes(Rttia).Tamanho <= 0))  then
      Exit(0);

    if DadosDefinicoes(Rttia).UtilizarPipe then
      SCampo := ALinha.Split(['|'])[DadosDefinicoes(Rttia).StartPos]
    else
      SCampo := Copy(ALinha,
                     DadosDefinicoes(Rttia).StartPos,
                     DadosDefinicoes(Rttia).Tamanho);
    SFormat := UpperCase(DadosDefinicoes(Rttia).DisplayFormat);
    if SFormat <> 'YYYYMMDDHHMMSS' then
      raise Exception.Create('Formata��o de Data/Hora n�o cadastrado!');
    Result := StringToDateTimeDef(SCampo, 0, SFormat);
  end;

  function _GetInteger: TValue;
  begin
    if (not DadosDefinicoes(Rttia).UtilizarPipe and
       (DadosDefinicoes(Rttia).StartPos <= 0) and
       (DadosDefinicoes(Rttia).Tamanho <= 0))  then
      Exit(0);

    if DadosDefinicoes(Rttia).UtilizarPipe then
      Result := StrToIntDef(ALinha.Split(['|'])[DadosDefinicoes(Rttia).StartPos], 0)
    else
      Result := StrToIntDef(Copy(ALinha,
                                 DadosDefinicoes(Rttia).StartPos,
                                 DadosDefinicoes(Rttia).Tamanho), 0);
  end;

  function _GetMoney: TValue;
  var
    SCampo: string;
  begin
    if (not DadosDefinicoes(Rttia).UtilizarPipe and
       (DadosDefinicoes(Rttia).StartPos <= 0) and
       (DadosDefinicoes(Rttia).Tamanho <= 0))  then
      Exit(0);

    if DadosDefinicoes(Rttia).UtilizarPipe then
      SCampo := ALinha.Split(['|'])[DadosDefinicoes(Rttia).StartPos]
    else
      SCampo := Copy(ALinha,
                     DadosDefinicoes(Rttia).StartPos,
                     DadosDefinicoes(Rttia).Tamanho);
    Result := StrToFloatDef(SCampo, 0) / 100;
  end;
begin
  RttiC := TRttiContext.Create;
  try
    RttiT := RttiC.GetType(AClass);
    for RttiF in RttiT.GetFields do
    begin
      if LowerCase('f' + AProperty) = LowerCase(RttiF.Name) then
      begin
        RttiA := TJeraDADIUtils.GetAttribute<DadosDefinicoes>(RttiF);
        if Assigned(RttiA) then
        begin
          case DadosDefinicoes(Rttia).TipoDado of
            fdatString:
              Exit(_GetString);
            fdatInteger:
              Exit(_GetInteger);
            fdatMoney:
              Exit(_GetMoney);
            fdatData:
              Exit(_GetData);
            fdatHora:
              Exit(_GetTime);
            fdatDataHora:
              Exit(_GetDataHora);
          end;
        end;
      end;
    end;
  finally
    RttiC.Free;
  end;
end;

class function TJeraDADIUtils.MontarLinhaFromObj(AObj: TObject): string;
var
  RttiC: TRttiContext;
  RttiT: TRttiType;
  RttiF: TRttiField;
  RttiP: TRttiProperty;
  RttiA: TCustomAttribute;
  RttiAE: TCustomAttribute;
  SValor: string;
  I: Integer;
begin
  Result := '';
  RttiC := TRttiContext.Create;
  try
    RttiT := RttiC.GetType(AObj.ClassType);
    for RttiF in RttiT.GetFields do
    begin
      RttiP := TJeraDADIUtils.PropertyFor(RttiF);
      RttiA := TJeraDADIUtils.GetAttribute<DadosDefinicoes>(RttiF);
      if Assigned(RttiA) then
      begin
        if DadosDefinicoes(Rttia).TipoDado = fdatString then
        begin
          if RttiP.PropertyType.TypeKind <> tkEnumeration then
          begin
            SValor := RttiP.GetValue(AObj).AsString;
            Result := Result + PadRight(SValor, DadosDefinicoes(Rttia).Tamanho);
          end
          else
          begin
            RttiAE := TJeraDADIUtils.GetAttribute<Enumerado>(RttiF);
            if Assigned(RttiAE) then
            begin
              if RttiP.PropertyType.Handle <> TypeInfo(Boolean) then
              begin
                I := RttiP.GetValue(AObj).AsOrdinal;
              end
              else
              begin
                I := RttiP.GetValue(AObj).AsOrdinal + 1;
              end;
              SValor := IfThen(Enumerado(RttiAE).Valores[I] <> '@', Enumerado(RttiAE).Valores[I], '');
              Result := Result + PadRight(SValor, DadosDefinicoes(Rttia).Tamanho);
            end;
          end;
        end;
        if DadosDefinicoes(Rttia).TipoDado = fdatData then
        begin
          if RttiP.GetValue(AObj).AsExtended > 0 then
            SValor := FormatDateTime(DadosDefinicoes(Rttia).DisplayFormat, RttiP.GetValue(AObj).AsExtended)
          else
            SValor := PadRight('0', Length(DadosDefinicoes(Rttia).DisplayFormat), '0');
          Result := Result  + SValor;
        end;
        if DadosDefinicoes(Rttia).TipoDado = fdatInteger then
        begin
          case RttiP.PropertyType.Handle.Kind of
            tkInteger,
              tkInt64: SValor := IntToStr(RttiP.GetValue(AObj).AsInt64);
            else
              SValor := RttiP.GetValue(AObj).AsString;
          end;
          Result := Result + PadLeft(SValor, DadosDefinicoes(Rttia).Tamanho, '0');
        end;
        if DadosDefinicoes(Rttia).TipoDado = fdatMoney then
        begin
          SValor := IntToStr(Trunc((RttiP.GetValue(AObj).AsExtended * 100)));
          Result := Result + PadLeft(SValor, DadosDefinicoes(Rttia).Tamanho, '0');
        end;
      end;
    end;
    Result := TiraAcentos(Result);
  finally
    RttiC.Free;
  end;
end;

class function TJeraDADIUtils.PropertyFor(const RttiF: TRttiField): TRttiProperty;
var
  SField, SProperty, SClass, SMsg: string;
begin
  Result := nil;
  try
    SField := RttiF.Name;
    SProperty := Copy(SField, 2, Length(SField));
    SClass := RttiF.Parent.AsInstance.MetaclassType.ClassName;
    Result := RttiF.Parent.GetProperty(SProperty);

    if not Assigned(Result) then
      raise Exception.Create('Property does not exists.');
  except
    on E: Exception do
    begin
      SMsg := Format('Property "%s" does not exists.' + sLineBreak + sLineBreak +
                     'Class: "%s"' + sLineBreak + sLineBreak +
                     'Field: "%s"' + sLineBreak + sLineBreak +
                     'Exception: %s ',
                     [sProperty, SClass, SField, E.Message]);

      raise Exception.Create(SMsg);
    end;
  end;
end;

{ TDADIClasseItemsBase<T> }

function TDADIClasseItemsBase<T>.Add(Obj: T): Integer;
begin
  Result := FLista.Add(Obj);
end;

procedure TDADIClasseItemsBase<T>.Clear;
var
  I: Integer;
  Obj: TObject;
begin
//  if not ItemIsInterface then
//  begin
//    for I := (Count - 1) downto 0 do
//    begin
//      Obj := TValue.From<T>(FLista.Items[I]).AsObject;
//      FreeAndNil(Obj);
//    end;
//  end;
  FLista.Clear;
end;

function TDADIClasseItemsBase<T>.Count: Integer;
begin
  Result := FLista.Count;
end;

constructor TDADIClasseItemsBase<T>.Create;
begin
  inherited;
  FLista := TList<T>.Create;
//  FItemIsInterface := True;
end;

function TDADIClasseItemsBase<T>.CreateObj(Classe: TClass): TObject;
var
  RttiC: TRttiContext;
  RttiT: TRttiType;
  RttiM: TRttiMethod;
begin
  Result := nil;
  try
    RttiT := RttiC.GetType(Classe);
    RttiM := RttiT.GetMethod('Create');
    if RttiM.IsConstructor then
      Result := RttiM.Invoke(Classe, []).AsObject;
  except
    raise Exception.Create('N�o foi poss�vel criar a classe: ' + Classe.ClassName);
  end;
end;

procedure TDADIClasseItemsBase<T>.Delete(Index: Integer);
begin
  FLista.Delete(Index);
end;

destructor TDADIClasseItemsBase<T>.Destroy;
begin
  Clear;
  FreeAndNil(FLista);
  inherited;
end;

function TDADIClasseItemsBase<T>.GetItem(Index: integer): T;
begin
  Result := FLista.Items[Index];
end;

Function TDADIClasseItemsBase<T>.New: T;
var
  Classe: TClass;
  OItem: TObject;
  ListItem: T;
  Value1, Value2: TValue;
begin
  OItem := CreateObj(FClazz);

  Value1 := OItem;
  Value1.TryCast(TypeInfo(T), Value2);

  ListItem := Value2.AsType<T>;

  Add(ListItem);
  Result := ListItem;
end;

{ ValorConstante }

constructor ValorConstante.Create(ATipoDado: TDADITipoDado; AValor: Variant);
begin
  FTipoDado := ATipoDado;
  FValor := AValor;
end;

end.
